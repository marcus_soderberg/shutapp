package com.example.shutapp;

import com.example.shutapp.enteties.message.Message;
import com.example.shutapp.enteties.message.MessageRepository;
import com.example.shutapp.enteties.user.AppUser;
import com.example.shutapp.enteties.user.AppUserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class ShutappApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShutappApplication.class, args);
    }

    @Bean
    CommandLineRunner init(AppUserRepository appUserRepository,
                           MessageRepository messageRepository) {
        return args -> {
            AppUser robin = new AppUser( "robban", "robin");
            AppUser marcus = new AppUser("mackan", "marcus");
            appUserRepository.saveAll(List.of(robin, marcus));
            Message message = new Message(robin.getId(), marcus.getId(), "Hej Mackan!");
            Message anotherMessage = new Message(marcus.getId(), robin.getId(), "Hej Robban!");
            Message aThirdMessage = new Message(robin.getId(), marcus.getId(), "Ska du med ner på stan?");
            Message aFourthMessage = new Message(marcus.getId(), robin.getId(), "Drar vi nu eller?");
            messageRepository.saveAll(List.of(message, anotherMessage, aThirdMessage, aFourthMessage));
        };
    }

}
