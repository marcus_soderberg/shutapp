package com.example.shutapp.enteties.message;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessageController {

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping
    public List<Message> getMessages() {
        return messageService.findAll();
    }

    @PostMapping
    public Message postMessage(@RequestBody Message newMessage) {
        return messageService.save(newMessage);
    }
}
