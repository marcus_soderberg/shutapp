package com.example.shutapp.enteties.user;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/app-users")
public class AppUserController {

    private final AppUserService appUserService;

    public AppUserController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @GetMapping
    public List<AppUser> getUsers() {
        return appUserService.findAll();
    }

    @PutMapping("/{id}")
    public AppUser updateUser(@PathVariable("id") int id, @RequestBody AppUser updatedAppUser) {
        return appUserService.update(id, updatedAppUser);
    }

}
