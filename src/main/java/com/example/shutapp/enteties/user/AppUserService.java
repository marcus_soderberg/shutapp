package com.example.shutapp.enteties.user;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppUserService {

    AppUserRepository appUserRepository;

    public AppUserService(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    public List<AppUser> findAll() {
        return appUserRepository.findAll();
    }

    public AppUser update(int id, AppUser updatedAppUser) {
        AppUser appUser = appUserRepository.findById(id).orElseThrow();


        if (!updatedAppUser.getUsername().isEmpty()) {
            appUser.setUsername(updatedAppUser.getUsername());
        }

        if (!updatedAppUser.getPassword().isEmpty()) {
            appUser.setPassword(updatedAppUser.getPassword());
        }

        return appUserRepository.save(appUser);
    }
}
