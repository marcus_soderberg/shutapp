import React from 'react';
import {
    SafeAreaView,
    StatusBar,
    View,
    Dimensions
} from 'react-native';
import LoginScreen from "./screens/LoginScreen";
import {NavigationContainer} from '@react-navigation/native';
import ChatScreen from "./screens/ChatScreen";
import {createDrawerNavigator, DrawerContentScrollView, DrawerItemList} from "@react-navigation/drawer";
import Header from "./components/Header";
import Icon from "react-native-vector-icons/Feather";
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";
import ProfileScreen from "./screens/ProfileScreen";


const closeMenu = (navigation) => {
    navigation.closeDrawer();
}

const CustomDrawerContent = (props) => (
    <DrawerContentScrollView>
        <View style={{
            marginLeft: 10,
            marginTop: 20,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between'
        }}>
            <Header>Meny</Header>
            <Pressable style={({pressed}) => [{opacity: pressed ? 0.5 : 1.0}]} onPress={() => closeMenu(props.navigation)}>
                <Icon name='x' style={{fontSize: 30, marginRight: 10, color: "white"}}/>
            </Pressable>
        </View>
        <DrawerItemList {...props} />
    </DrawerContentScrollView>
);

const drawerOptions = {
    drawerStyle: {
        width: Dimensions.get('screen').width,
        backgroundColor: 'rgb(114,111,111)',
        borderRightWidth: 1,
        borderRightColor: '#000',
    },
    drawerLabelStyle: {
        color: '#FFF',
        fontSize: 20,
    },
    drawerActiveBackgroundColor: 'rgb(75,68,68)',
};

const Drawer = createDrawerNavigator();
const hidden = {drawerItemStyle: {display: 'none'}};

export default function App() {


    return (
        <NavigationContainer>
            <Drawer.Navigator
                initialRouteName="LoginScreen"
                screenOptions={{headerShown: false, ...drawerOptions}}
                drawerContent={(props) => <CustomDrawerContent {...props} />}
            >
                <Drawer.Screen name='LoginScreen' options={{unmountOnBlur: true, ...hidden}} component={LoginScreen}/>
                <Drawer.Screen name='ChatScreen' options={{unmountOnBlur: true, title: 'Chatt'}} component={ChatScreen}/>
                <Drawer.Screen name='ProfileScreen' options={{title: 'Din profil'}} component={ProfileScreen} />
            </Drawer.Navigator>
            <SafeAreaView>
                <StatusBar/>
            </SafeAreaView>
        </NavigationContainer>
    );
};
