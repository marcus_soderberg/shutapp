import React from 'react';
import {Pressable, Text, StyleSheet} from "react-native";

const Button = ({onPress, children}) => {
    return (
        <Pressable style={({pressed}) => [styles.button, {opacity: pressed ? 0.5 : 1.0}]} onPress={onPress}>
            <Text style={styles.buttonText}>{children}</Text>
        </Pressable>
    );
}

const styles = StyleSheet.create({
   button: {
       borderWidth: 1,
       borderColor: "black",
       padding: 10,
       width: "30%",
       backgroundColor: "#3b021f",
       borderRadius: 6,
       elevation: 5
   },
    buttonText: {
        textAlign: "center",
        color: "white",
    }
});

export default Button;
