import React from 'react';
import {StyleSheet, Text, View} from "react-native";

export const SenderMessage = ({ message }) => {
    return (
        <View style={[styles.messageContainerLeft, styles.messageContainer]}>
            <Text style={styles.messageText}>{message.message}</Text>
        </View>
    );
}

export const ReceiverMessage = ({ message }) => {
    return (
        <View style={[styles.messageContainerRight, styles.messageContainer]}>
            <Text style={styles.messageText}>{message.message}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    messageContainerLeft: {
        alignSelf: 'flex-start',
        backgroundColor: "#3b021f",
        elevation: 6
    },
    messageContainerRight: {
        alignSelf: 'flex-end',
        backgroundColor: "rgb(0, 132, 255)",
        elevation: 6
    },
    messageContainer: {
        padding: 15,
        borderRadius: 6,
        margin: 10,
    },
    messageText: {
        color: "white",
    },
});

