import React from 'react';
import {Text, StyleSheet} from "react-native";

const Header = ({children}) => {
    return <Text style={style.headerText}>{children}</Text>;
}

const style = StyleSheet.create({
    headerText: {
        fontSize: 30
    }
});

export default Header;
