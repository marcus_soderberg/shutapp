import React from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';

const Loading = () => (
    <View style={styles.spinner}>
        <ActivityIndicator color='rgb(75,68,68)' size="large" />
    </View>
);

const styles = StyleSheet.create({
    spinner: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        height: '100%',
        flex: 1,
    },
});

export default Loading;
