import ToastAndroid from 'react-native/Libraries/Components/ToastAndroid/ToastAndroid';

export const Toast = (message) => {
    ToastAndroid.show(
        message,
        ToastAndroid.LONG,
    );
}
