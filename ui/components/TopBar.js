import React from "react";
import {Text, View, StyleSheet} from "react-native";
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";
import Icon from "react-native-vector-icons/Feather";

const TopBar = (props) => {

    const openMenu = () => {
        props.navigation.openDrawer();
    }

    return (
        <View style={styles.topBarContainer}>
            <Pressable style={({pressed}) => [styles.button, {opacity: pressed ? 0.5 : 1.0}]} onPress={() => openMenu()}>
                <Icon name="menu" style={styles.icon}/>
            </Pressable>
            <View style={styles.topBarTextContainer}>
                <Text style={styles.topBarText}>{props.topBarText}</Text>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    topBarContainer: {
        height: 60,
        backgroundColor: 'rgb(75,68,68)',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    icon: {
        fontSize: 40,
        marginLeft: 10,
        color: '#FFF',
    },
    topBarTextContainer: {
      flex: 1,
      marginRight: 50
    },
    topBarText: {
        textAlign: "center",
        fontSize: 20,
        fontWeight: "bold",
        color: '#FFF'
    }
})

export default TopBar;
