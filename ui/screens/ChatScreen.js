import React, {useCallback, useState, useRef} from "react";
import {Text, View, StyleSheet, TextInput, ScrollView, Keyboard} from "react-native";
import {fetchAppUsers, fetchMessages, getAppUserFromPhoneStorage, postMessage} from "../utils/network";
import {useFocusEffect} from "@react-navigation/native";
import Pressable from "react-native/Libraries/Components/Pressable/Pressable";
import TopBar from "../components/TopBar";
import Loading from "../components/Loading";
import Icon from "react-native-vector-icons/Feather";
import {ReceiverMessage, SenderMessage} from "../components/ChatMessage";


const ChatScreenContent = (props) => {

    return props.messages.map((message) => {
        if (message.senderId !== props.appUser.id) {
            return <SenderMessage key={message.id} message={message}/>;
        } else {
            return <ReceiverMessage key={message.id} message={message}/>;
        }
    });

}

const ChatScreen = (props) => {
    const [appUser, setAppUser] = useState(null);
    const [messages, setMessages] = useState([]);
    const [chatMessage, setChatMessage] = useState("");
    const [appUsers, setAppUsers] = useState([]);

    const scrollViewRef = useRef();

    useFocusEffect(
        useCallback(() => {
            getAppUserFromPhoneStorage()
                .then(user => setAppUser(user));
            fetchMessages()
                .then(m => setMessages(m.sort((a, b) => a - b)));
            fetchAppUsers()
                .then(users => setAppUsers(users));
            return () => {

            }
        }, []),
    );

    const handleSubmit = () => {
        postMessage(chatMessage, appUser, appUsers)
            .then(() => {
                setChatMessage("");
                fetchMessages()
                    .then(m => setMessages(m));
            });
        Keyboard.dismiss();
    }

    if (!appUser || !messages || !appUsers.length) {
        return <Loading/>
    }

    return (
        <View style={styles.chatContainer}>
            <TopBar
                navigation={props.navigation}
                topBarText={`Konversation med ${appUsers.find(user => user.id !== appUser.id).username}`}
            />
            <ScrollView
                ref={scrollViewRef}
                onContentSizeChange={() => scrollViewRef.current.scrollToEnd({animated: true})}
            >
                <ChatScreenContent
                    messages={messages}
                    appUser={appUser}
                />
            </ScrollView>
            <View style={styles.newMessageContainer}>
                <TextInput
                    style={styles.messageInput}
                    placeholder="Skriv ditt meddelande..."
                    placeholderTextColor={"#ffffff"}
                    onChangeText={(chatMessage) => setChatMessage(chatMessage)}
                    value={chatMessage}
                    multiline={true}
                />
                <Pressable style={({pressed}) => [styles.sendButton, {opacity: pressed ? 0.5 : 1.0}]} onPress={() => handleSubmit()}>
                    <Icon name='send' style={styles.sendButtonIcon}/>
                </Pressable>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({
    chatContainer: {
        flex: 1,
        borderBottomWidth: 3,
        backgroundColor: "rgb(255,255,255)"
    },
    chatContainerText: {
        textAlign: "center",
        marginTop: 10,
        marginBottom: 50,
        fontSize: 20,
        fontWeight: "bold",
    },
    messageInput: {
        backgroundColor: "rgb(75,68,68)",
        margin: 5,
        borderRadius: 6,
        color: "white",
        elevation: 5,
        minWidth: 300,
        paddingHorizontal: 20
    },
    newMessageContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        borderTopWidth: 1,
        borderColor: "black",
        backgroundColor: "rgb(61,56,56)",
    },
    sendButton: {
        flex: 1,
        alignItems: 'center'
    },
    sendButtonIcon: {
        fontSize: 30,
        color: '#FFF'
    }
})

export default ChatScreen;
