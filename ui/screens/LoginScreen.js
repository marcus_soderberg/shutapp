import React, {useCallback, useState} from "react";
import {fetchAppUsers, saveAppUserToPhoneStorage} from "../utils/network";
import {TextInput, View, ImageBackground, StyleSheet} from "react-native";
import {useFocusEffect} from "@react-navigation/native";
import Loading from "../components/Loading";
import Button from "../components/Button";
import {Toast} from "../components/Toast";


const LoginScreen = (props) => {
    const [appUsers, setAppUsers] = useState([]);
    const [usernameInput, setUsernameInput] = useState("");
    const [userPasswordInput, setUserPasswordInput] = useState("");


    const handleLogin = () => {
        const user = appUsers
            .find(appUser =>
                appUser.username === usernameInput &&
                appUser.password === userPasswordInput);

        if (!user) {
            Toast('Ogiltigt användarnamn eller lösenord.');
            return;
        }

        saveAppUserToPhoneStorage(user);

        props.navigation.navigate('ChatScreen');
    }

    useFocusEffect(
        useCallback(() => {
            fetchAppUsers()
                .then(users => setAppUsers(users));

            return () => {

            }
        }, []),
    );


    if (!appUsers) {
        return <Loading />
    }


    return (
        <View style={styles.loginContainer}>
            <ImageBackground style={styles.backgroundImage} source={require("../images/background.jpg")} resizeMode={"cover"}>
                <View style={styles.inputContainer}>
                    <TextInput
                        style={styles.input}
                        placeholder="Användarnamn"
                        onChangeText={(text) => setUsernameInput(text)}
                        value={usernameInput}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Lösenord"
                        onChangeText={(text) => setUserPasswordInput(text)}
                        value={userPasswordInput}
                        secureTextEntry={true}
                    />
                    <Button onPress={() => handleLogin()}>
                        Logga in
                    </Button>
                </View>
            </ImageBackground>

        </View>
    );



}

const styles = StyleSheet.create({
    loginContainer: {
        flex: 1,
        backgroundColor: "black"
    },
    backgroundImage: {
      backgroundColor: "black",
        flex: 1
    },
    inputContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 100
    },
    input: {
        backgroundColor: "rgba(255,255,255, 0.5)",
        width: "50%",
        borderRadius: 6,
        marginBottom: 30,
        padding: 4
    }
})

export default LoginScreen;
