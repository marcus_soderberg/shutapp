import React, {useCallback, useState} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import TopBar from "../components/TopBar";
import Button from "../components/Button";
import {useFocusEffect} from "@react-navigation/native";
import {
    getAppUserFromPhoneStorage,
    saveAppUserToPhoneStorage,
    updateAppUser
} from "../utils/network";
import Loading from "../components/Loading";
import {Toast} from "../components/Toast";

const ProfileScreen = (props) => {
    const [ appUser, setAppUser ] = useState(null);
    const [ newUsername, setNewUsername ] = useState("");
    const [ newPassword, setNewPassword ] = useState("");

    const handleSubmit = () => {
        const updatedAppUser = {
            id: appUser.id,
            username: newUsername ? newUsername : appUser.username,
            password: newPassword ? newPassword : appUser.password
        }

        updateAppUser(updatedAppUser)
            .then(user => saveAppUserToPhoneStorage(user)
                .then(() => {
                    setNewUsername("");
                    setNewPassword("");
                }));
        if (newUsername || newPassword) {
            Toast('Dina ändringar har registrerats.');
        }
    }

    useFocusEffect(
        useCallback(() => {
            getAppUserFromPhoneStorage()
                .then(user => setAppUser(user));
            return () => {

            }
        }, []),
    );

    if (!appUser) {
        return <Loading />
    }

    return(
        <View style={styles.container}>
            <TopBar
                navigation={props.navigation}
                topBarText={'Redigera din profil'}
            />
            <View style={styles.inputsContainer}>
            <Text style={styles.inputTitle}>Ändra ditt användarnamn:</Text>
            <TextInput
                style={styles.input}
                onChangeText={text => setNewUsername(text)}
                value={newUsername}
            />
            <Text style={styles.inputTitle}>Ändra ditt lösenord:</Text>
            <TextInput
                style={styles.input}
                onChangeText={text => setNewPassword(text)}
                value={newPassword}
            />
            <Button onPress={() => handleSubmit()}>
                Spara
            </Button>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    inputsContainer: {
      alignItems: "center",
      height: 100,
      justifyContent: "center",
      flex: 1,
    },
    input: {
        backgroundColor: "rgba(255,255,255, 0.5)",
        width: "50%",
        borderRadius: 6,
        marginBottom: 30,
        padding: 4,
        borderWidth: 1,
    },
    inputTitle: {
        fontWeight: "bold",
        paddingBottom: 3
    }
});

export default ProfileScreen;
