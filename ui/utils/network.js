import AsyncStorage from "@react-native-async-storage/async-storage";
import { BASE_URL } from '@env';

export const fetchAppUsers = async () => {
    const response = await fetch(`${BASE_URL}/app-users`);

    return await response.json();
}

export const fetchMessages = async () => {
    const response = await fetch(`${BASE_URL}/messages`);

    return await response.json();
}

export const updateAppUser = async (appUser) => {
    const response = await fetch(`${BASE_URL}/app-users/${appUser.id}`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(appUser)
        });

    return await response.json();
}

export const saveAppUserToPhoneStorage = async (appUser) => {
    const appUserData = JSON.stringify(appUser);
    return AsyncStorage.setItem('appUser', appUserData);
}

export const getAppUserFromPhoneStorage = async () => {
    const appUser = await AsyncStorage.getItem('appUser');

    if (!appUser) {
        return;
    }

    return JSON.parse(appUser);
}

export const postMessage = async (message, appUser, appUsers) => {

    const receiverId = appUsers.find(user => user.id !== appUser.id)

    const newMessage = {
        senderId: appUser.id,
        receiverId: receiverId.id,
        message: message,
    }
    
    const response = await fetch(`${BASE_URL}/messages`,
        {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(newMessage),
    });

    return await response.json();
}

export const fetchConversations = async () => {
    const response = await fetch(`${BASE_URL}/conversations`);

    return await response.json();
}
